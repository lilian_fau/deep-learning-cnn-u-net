# Importation des bibliothèques
# Les bibliothèques «os», «random», et «re» sont utilisées pour des opérations liées au système, à la génération de nombres aléatoires et à l'utilisation d'expressions régulières.
# Les fonctions de la bibliothèque «skimage.io» sont utilisées pour lire, afficher et sauvegarder des images.
# La fonction «resize» de la bibliothèque «skimage.transform» est utilisée pour redimensionner les images.
import os
import random
import re
from skimage.io import imread, imshow, imsave
from skimage.transform import resize

# Définition des chemins et paramètres
# «DATA_PATH» -> chemin du répertoire principal des données.
# «FRAME_PATH» -> chemin du répertoire contenant les images IRM.
# «MASK_PATH» -> chemin du répertoire contenant les masques correspondants.
# «zmin», «zmax», «tmin», «tmax» -> plages attendues pour les niveaux de tranche et de temps.
# «IMG_WIDTH» et «IMG_HEIGHT» -> taille des images de sortie.
DATA_PATH = 'C:/Users/lasarry.UCA/Documents/UE3.8'
FRAME_PATH = DATA_PATH + '/frames'
MASK_PATH = DATA_PATH + '/masks'

# Valeurs minimales et maximales pour les niveaux de tranche et le temps
zmin = 0.1
zmax = 0.9
tmin = 0.0
tmax = 1.0

# Taille de l'image
IMG_WIDTH = 128
IMG_HEIGHT = 128

# Création des dossiers
# Six dossiers sont créés pour stocker les images d'entraînement, de validation et de test, ainsi que les masques correspondants.
folders = ['train_frames/Train', 'train_masks/Train', 'val_frames/Val', 'val_masks/Val', 'test_frames/Test', 'test_masks/Test']

for folder in folders:
    if not os.path.exists(DATA_PATH + '/' + folder):
        os.makedirs(DATA_PATH + '/' + folder)

# Récupération de tous les noms de fichiers d'images et de masques
all_frames = os.listdir(FRAME_PATH)
all_masks = os.listdir(MASK_PATH)

# Sélection des masques pertinents
# Les noms de fichiers des masques sont filtrés en utilisant une expression régulière
# pour s'assurer qu'ils se situent dans les plages spécifiées pour z et t.
# La sélection des masques dans la plage attendue de z et t est effectuée en utilisant une expression régulière en Python.
# Vous pouvez utiliser le site Web https://regex101.com/ pour tester votre expression régulière.
# Le résultat est une liste de noms de fichiers de masques sélectionnés (selected_masks)
regex = "\d\.\d+"
selected_masks = []
for mask in all_masks:
    match = re.findall(regex, mask)
    z = float(match[0])
    t = float(match[1])
    if z > zmin and z < zmax and t > tmin and t < tmax:
        selected_masks.append(mask)

# Mélange aléatoire et partitionnement des masques
# Les masques sélectionnés sont mélangés de manière aléatoire, puis répartis en ensembles d'entraînement, de validation et de test selon des proportions prédéfinies.
random.shuffle(selected_masks)

# Générer des listes de noms de fichiers de masques pour les ensembles d'entraînement, de validation et de test
# La liste d'entraînement correspond aux 70% premiers,
# la liste de validation correspond aux 20% suivants,
# la liste de test correspond aux 10% derniers.
end_train = int(0.7 * len(selected_masks))
end_val = int(0.9 * len(selected_masks))
train = selected_masks[:end_train]
val = selected_masks[end_train:end_val]
test = selected_masks[end_val:]

# Ajouter les images d'entraînement, de validation et de test ainsi que les masques aux dossiers correspondants
frame_folders = [(train, 'train_frames/Train'), (val, 'val_frames/Val'), 
                 (test, 'test_frames/Test')]

mask_folders = [(train, 'train_masks/Train'), (val, 'val_masks/Val'), 
                (test, 'test_masks/Test')]

# Fonctions pour le traitement des images
# Deux sous-routines («add_frames» et «add_masks») sont définies pour lire, redimensionner et sauvegarder les images et les masques.
# Sous-routine qui lit l'image depuis FRAME_PATH,
# la redimensionne à IMG_HEIGHT x IMG_WIDTH
# et la sauvegarde dans le sous-répertoire dir_name de DATA_PATH
def add_frames(dir_name, image):
    array = imread(FRAME_PATH + '/' + image)
    array = resize(array, (IMG_HEIGHT, IMG_WIDTH))
    imsave(DATA_PATH + '/' + dir_name + '/' + image, array)

# Sous-routine qui lit le masque depuis MASK_PATH,
# le redimensionne à IMG_HEIGHT x IMG_WIDTH
# et la sauvegarde dans le sous-répertoire dir_name de DATA_PATH
def add_masks(dir_name, mask):
    array = imread(MASK_PATH + '/' + mask)
    array = resize(array, (IMG_HEIGHT, IMG_WIDTH))
    imsave(DATA_PATH + '/' + dir_name + '/' + mask, array)

# Ajouter les images
for folder in frame_folders:
    array = folder[0]
    name = [folder[1]] * len(array)
    list(map(add_frames, name, array))

# Ajouter les masques
for folder in mask_folders:
    array = folder[0]
    name = [folder[1]] * len(array)
    list(map(add_masks, name, array))