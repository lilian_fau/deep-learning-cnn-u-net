import tensorflow.keras.backend as K
import numpy
from scipy.ndimage import _ni_support
from scipy.ndimage import distance_transform_edt, binary_erosion,\
    generate_binary_structure

# Coefficient de Dice
# La fonction «dice_coef» calcule le coefficient de Dice entre deux masques binaires (vrai et prédit). Le coefficient de Dice mesure la similarité entre deux ensembles et est défini comme «(2 * intersection) / (cardinalité du vrai ensemble + cardinalité du prédit ensemble)».
# Permet à partir d’image binaire de calculer la similarité entre l’images prédite (vecteur pred = prend pred) par le modèle et l’image vrais (vecteur true = prend Masks) pixel par pixel. Donc, la zone de chevauchement (multipliée par 2) est divisée par le nombre total de pixels dans les deux images.

# Sous-routine qui calcule le coefficient de Dice
# à partir d'images binaires vraies et prédites
def dice_coef(y_true, y_pred):
    y_true_vec = K.flatten(y_true)
    y_pred_vec = K.flatten(y_pred)
    intersection = y_true_vec * y_pred_vec
    y_true_card = K.sum(y_true_vec)
    y_pred_card = K.sum(y_pred_vec)
    if y_true_card == 0 and y_pred_card == 0:
        return 0
    else:
        return 2 * K.sum(intersection) / (y_true_card + y_pred_card)

# Perte de coefficient de Dice
# La fonction «dice_coef_loss» calcule la perte associée au coefficient de Dice. Elle est définie comme «1 - dice_coef(y_true, y_pred)».
# Sous-routine qui calcule la perte de coefficient de Dice
def dice_coef_loss(y_true, y_pred):
    return 1. - dice_coef(y_true, y_pred)

# Distance de Hausdorff
# La distance de Hausdorff est la distance maximale entre n'importe quel point du premier ensemble et son point le plus proche du deuxième ensemble, et vice-versa.
# Ici, entre deux ensembles de points X et Y. Implémentée en deux étapes : hd1 puis hd2 (calcul distance entre résultats = image prédite et référence = l’image vraie), puis on prend le maximum. L’objectif est de minimiser cette distance.
# La fonction «hd» calcule la distance de Hausdorff entre deux masques binaires. La distance de Hausdorff est la distance maximale entre les surfaces des objets. Cette fonction utilise la distance euclidienne transformée («distance_transform_edt») pour calculer la distance de surface.

def hd(result, reference, voxelspacing=None, connectivity=1):
    """
    Distance de Hausdorff.
    
    Calcule la distance de Hausdorff (HD) (symétrique) entre les objets binaires dans deux
    images. Elle est définie comme la distance maximale de surface entre les objets.
    
    Paramètres
    ----------
    result : array_like
        Données d'entrée contenant des objets. Peut être de n'importe quel type, mais sera converti
        en binaire : fond où 0, objet partout ailleurs.
    reference : array_like
        Données d'entrée contenant des objets. Peut être de n'importe quel type, mais sera converti
        en binaire : fond où 0, objet partout ailleurs.
    voxelspacing : float ou séquence de floats, optionnel
        L'espacement des voxels dans une unité de distance, c'est-à-dire l'espacement des éléments
        le long de chaque dimension. Si une séquence, elle doit avoir une longueur égale à
        le rang d'entrée ; si un seul nombre, il est utilisé pour toutes les axes. Si
        non spécifié, un espacement de grille de 1 est sous-entendu.
    connectivity : int
        La connectivité considérée lors de la détermination de la surface
        des objets binaires. Cette valeur est transmise à
        `scipy.ndimage.generate_binary_structure` et devrait généralement être :math:`> 1`.
        Notez que la connectivité influence le résultat dans le cas de la distance de Hausdorff.
        
    Retour
    -------
    hd : float
        La distance de Hausdorff symétrique entre l'objet dans ```result``` et l'objet dans
        ```reference```. L'unité de distance est la même que pour l'espacement des
        éléments le long de chaque dimension, qui est généralement donné en mm.
        
    Voir aussi
    --------
    :func:`assd`
    :func:`asd`
    
    Remarques
    -----
    Il s'agit d'une vraie métrique. Les images binaires peuvent donc être fournies dans n'importe quel ordre.
    """
    hd1 = __surface_distances(result, reference, voxelspacing, connectivity).max()
    hd2 = __surface_distances(reference, result, voxelspacing, connectivity).max()
    hd = max(hd1, hd2)
    return hd

# Distance de Surface Symétrique Moyenne (ASSD)
# La métrique ASSD est la moyenne de toutes les distances entre les points situés à la limite de la région segmentée par la machine et la limite de la vérité terrain (on s’intéresse aux voxels donc). En effet, cette métrique permet de résoudre le problème rencontré avec Dice pour lequel : nous pourrions avoir un bon ‘coefficient de DICE’ → mais c’est juste par ce que y a une superposition d’un contour d’une image sur toute une autre image. C'est pourquoi on utilise le ASSD qui calcule la distance moyenne entre contours (cherche les pixels les plus proches). L’objectif est de minimiser cette distance.
# La fonction «

assd» calcule la distance de surface symétrique moyenne entre deux masques binaires. Elle utilise la même approche que la distance de Hausdorff, mais en calculant la moyenne des distances de surface symétriques.

def assd(result, reference, voxelspacing=None, connectivity=1):
    """
    Distance de surface symétrique moyenne.
    
    Calcule la distance de surface symétrique moyenne (ASD) entre les objets binaires dans
    deux images.
    
    Paramètres
    ----------
    result : array_like
        Données d'entrée contenant des objets. Peut être de n'importe quel type, mais sera converti
        en binaire : fond où 0, objet partout ailleurs.
    reference : array_like
        Données d'entrée contenant des objets. Peut être de n'importe quel type, mais sera converti
        en binaire : fond où 0, objet partout ailleurs.
    voxelspacing : float ou séquence de floats, optionnel
        L'espacement des voxels dans une unité de distance, c'est-à-dire l'espacement des éléments
        le long de chaque dimension. Si une séquence, elle doit avoir une longueur égale à
        le rang d'entrée ; si un seul nombre, il est utilisé pour toutes les axes. Si
        non spécifié, un espacement de grille de 1 est sous-entendu.
    connectivity : int
        La connectivité considérée lors de la détermination de la surface
        des objets binaires. Cette valeur est transmise à
        `scipy.ndimage.generate_binary_structure` et devrait généralement être :math:`> 1`.
        La décision sur la connectivité est importante, car elle peut influencer les résultats
        fortement. En cas de doute, laissez-le tel quel.         
        
    Retour
    -------
    assd : float
        La distance de surface symétrique moyenne entre l'objet dans ``result`` et l'objet dans
        ``reference``. L'unité de distance est la même que pour l'espacement des
        éléments le long de chaque dimension, qui est généralement donné en mm.
        
    Voir aussi
    --------
    :func:`asd`
    :func:`hd`
    
    Remarques
    -----
    Il s'agit d'une vraie métrique, obtenue en appelant
    
    >>> __surface_distances(result, reference)
    
    et
    
    >>> __surface_distances(reference, result)
    
    puis en faisant la moyenne des deux listes. Les images binaires peuvent donc être fournies dans n'importe quel ordre.
    """
    assd1 = numpy.mean(__surface_distances(result, reference, voxelspacing, connectivity))
    assd2 = numpy.mean(__surface_distances(reference, result, voxelspacing, connectivity))
	
    return 0.5 * (assd1 + assd2)

# Distances de Surface
# La sous-routine «__surface_distances» calcule les distances entre les voxels de surface des objets binaires dans le résultat et leur voxel de surface partenaire le plus proche d'un objet binaire dans la référence. Elle est utilisée par «hd» et «assd».

def __surface_distances(result, reference, voxelspacing=None, connectivity=1):
    """
    Les distances entre le voxel de surface d'objets binaires dans le résultat et leur
    voxel de surface partenaire le plus proche d'un objet binaire dans la référence.
    """
    result = numpy.atleast_1d(result.astype(numpy.bool))
    reference = numpy.atleast_1d(reference.astype(numpy.bool))
    if voxelspacing is not None:
        voxelspacing = _ni_support._normalize_sequence(voxelspacing, result.ndim)
        voxelspacing = numpy.asarray(voxelspacing, dtype=numpy.float64)
        if not voxelspacing.flags.contiguous:
            voxelspacing = voxelspacing.copy()
            
    # Structure binaire
    footprint = generate_binary_structure(result.ndim, connectivity)
    
    # Test d'absence
    if 0 == numpy.count_nonzero(result): 
        raise RuntimeError("Le premier tableau fourni ne contient aucun objet binaire.")
    if 0 == numpy.count_nonzero(reference): 
        raise RuntimeError("Le deuxième tableau fourni ne contient aucun objet binaire.")    
            
    # Extraction uniquement de la ligne de bord 1 pixel des objets
    result_border = result ^ binary_erosion(result, structure=footprint, iterations=1)
    reference_border = reference ^ binary_erosion(reference, structure=footprint, iterations=1)
    
    # Calcul de la distance de surface moyenne        
    # Remarque : la transformée de distance de scipy est calculée uniquement à l'intérieur des bords de
    #       des objets en premier plan, donc l'entrée doit être inversée
    dt = distance_transform_edt(~reference_border, sampling=voxelspacing)
    sds = dt[result_border]
    
    return sds