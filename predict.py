# Importation des bibliothèques
# Importe les bibliothèques nécessaires, y compris des modules personnalisés pour le modèle, les métriques et les pertes, ainsi que des bibliothèques pour les opérations sur les images.

import os
import numpy as np
from skimage.io import imread, imsave
from skimage.util import img_as_float
from skimage.transform import resize
from skimage.io import imshow
from matplotlib import pyplot as plt

import model
import metrics_and_losses
from medpy.metric.binary import asd, hd

# Définition des chemins des données
# Spécifie les chemins vers les répertoires contenant les images d'entraînement, de validation et de test, ainsi que les masques correspondants.

DATA_PATH = 'C:/Users/lasarry.UCA/Documents/UE3.8'

TRAIN_FRAME_PATH = os.path.join(DATA_PATH, 'train_frames/Train')
VAL_FRAME_PATH = os.path.join(DATA_PATH, 'val_frames/Val')
TEST_FRAME_PATH = os.path.join(DATA_PATH, 'test_frames/Test')

TRAIN_MASK_PATH = os.path.join(DATA_PATH, 'train_masks/Train')
VAL_MASK_PATH = os.path.join(DATA_PATH, 'val_masks/Val')
TEST_MASK_PATH = os.path.join(DATA_PATH, 'test_masks/Test')

# En raison de l'augmentation géométrique des images, les images générées sont deux fois plus grandes que les images initiales
IMG_WIDTH = 256
IMG_HEIGHT = 256

# Chargement du modèle entraîné
# Charge un modèle U-Net avec les poids préalablement entraînés.

# Charge le modèle avec les poids optimisés
m = model.get_UNet(256, 256)
m.summary()
m.load_weights('weights_CE_100_64_0.04144_endo.h5')

# Chargement des données d'images
# Charge les images d'entraînement, de validation et de test dans des tableaux NumPy, en redimensionnant les images à la taille spécifiée.

# Construit une matrice numpy 4D pour stocker toutes les images d'entraînement à partir de TRAIN_FRAME_PATH
# sortie = img_train avec les indices 0 pour le numéro de l'image, 1 pour l'ordonnée, 2 pour l'abscisse, 3 pour le canal (0 pour les images en niveaux de gris)
train_frames = os.listdir(TRAIN_FRAME_PATH)
img_train = np.empty((len(train_frames), IMG_HEIGHT, IMG_WIDTH, 1))
for n, frame in enumerate(train_frames):
    img = imread(os.path.join(TRAIN_FRAME_PATH, frame))
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH))
    img_train[n, :, :, 0] = img

# Construit une matrice numpy 4D pour stocker toutes les images de validation à partir de VAL_FRAME_PATH
# sortie = img_val avec les indices 0 pour le numéro de l'image, 1 pour l'ordonnée, 2 pour l'abscisse, 3 pour le canal (0 pour les images en niveaux de gris)
val_frames = os.listdir(VAL_FRAME_PATH)
img_val = np.empty((len(val_frames), IMG_HEIGHT, IMG_WIDTH, 1))
for n, frame in enumerate(val_frames):
    img = imread(os.path.join(VAL_FRAME_PATH, frame))
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH))
    img_val[n, :, :, 0] = img

# Construit une matrice numpy 4D pour stocker toutes les images de test à partir de TEST_FRAME_PATH
# sortie = img_test avec les indices 0 pour le numéro de l'image, 1 pour l'ordonnée, 2 pour l'abscisse, 3 pour le canal (0 pour les images en niveaux de gris)
test_frames = os.listdir(TEST_FRAME_PATH)
img_test = np.empty((len(test_frames), IMG_HEIGHT, IMG_WIDTH, 1))
for n, frame in enumerate(test_frames):
    img = imread(os.path.join(TEST_FRAME_PATH, frame))
    img = resize(img, (IMG_HEIGHT, IMG_WIDTH))
    img_test[n, :, :, 0] = img

# Chargement des données d'images
# Charge les images d'entraînement, de validation et de test dans des tableaux NumPy, en redimensionnant les images à la taille spécifiée.

# Prédiction à partir du modèle m
pred_train = m.predict(img_train, verbose=1) > 0.5
pred_val = m.predict(img_val, verbose=1) > 0.5
pred_test = m.predict(img_test, verbose=1) > 0.5

# Fusion entre les images initiales et les masques prédits à des fins d'affichage
display_train = pred_train + img_train
display_val = pred_val + img_val
display_test = pred_test + img_test

# Chemins pour enregistrer les images prédites
TRAIN_PRED_PATH = os.path.join(DATA_PATH, 'train_preds')
if not os.path.exists(TRAIN_PRED_PATH):
    os.makedirs(TRAIN_PRED_PATH)
VAL_PRED_PATH = os.path.join(DATA_PATH, 'val_preds')
if not os.path.exists(VAL_PRED_PATH):
    os.makedirs(VAL_PRED_PATH)
TEST_PRED_PATH = os.path.join(DATA_PATH, 'test_preds')
if not os.path.exists(TEST_PRED_PATH):
    os.makedirs(TEST_PRED_PATH)

# Fusion des images prédites avec les images initiales
# Combine les images prédites avec les images d'origine pour visualisation.

# Enregistre la fusion des images d'entraînement et des masques prédits correspondants dans TRAIN_PRED_PATH
# Calcule le coefficient de Dice moyen pour l'ensemble d'entraînement
train_mean_dice = 0
train_mean_asd = 0
train_mean_hd = 0
m = 0
for n, frame in enumerate(train_frames):
    img = 127 * display_train[n, :, :, 0]
    img = img.astype(np.uint8)
    imsave(os.path.join(TRAIN_PRED_PATH, frame), img)
    mask = imread(os.path.join(TRAIN_MASK_PATH, frame))
    mask = resize(mask, (IMG_HEIGHT, IMG_WIDTH))
    pred = pred_train[n, :, :, 0].astype(np.float64)
    if not np.amax(pred) == 0:
        dice = metrics_and_losses.dice_coef(mask, pred)
        train_mean_dice += dice
        if not np.amax(mask) == 0:
            euclidian = asd(pred, mask)
            train_mean_asd += euclidian
            hausdorff = hd(pred, mask)
            train_mean

_hd += hausdorff
            m += 1
train_mean_dice /= m
print("Coefficient de Dice moyen pour l'ensemble d'entraînement= {}".format(train_mean_dice))
train_mean_asd /= m
print("Distance euclidienne moyenne pour l'ensemble d'entraînement= {}".format(train_mean_asd))
train_mean_hd /= m
print("Distance de Hausdorff moyenne pour l'ensemble d'entraînement= {}".format(train_mean_hd))

# Enregistre la fusion des images de validation et des masques prédits correspondants dans VAL_PRED_PATH
# Calcule le coefficient de Dice moyen pour l'ensemble de validation
val_mean_dice = 0
val_mean_asd = 0
val_mean_hd = 0
m = 0
for n, frame in enumerate(val_frames):
    img = 127 * display_val[n, :, :, 0]
    img = img.astype(np.uint8)
    imsave(os.path.join(VAL_PRED_PATH, frame), img)
    mask = imread(os.path.join(VAL_MASK_PATH, frame))
    mask = resize(mask, (IMG_HEIGHT, IMG_WIDTH))
    pred = pred_val[n, :, :, 0].astype(np.float64)
    if not np.amax(pred) == 0:
        dice = metrics_and_losses.dice_coef(mask, pred)
        val_mean_dice += dice
        if not np.amax(mask) == 0:
            euclidian = asd(pred, mask)
            val_mean_asd += euclidian
            hausdorff = hd(pred, mask)
            val_mean_hd += hausdorff
            m += 1
val_mean_dice /= m
print("Coefficient de Dice moyen pour l'ensemble de validation= {}".format(val_mean_dice))
val_mean_asd /= m
print("Distance euclidienne moyenne pour l'ensemble de validation= {}".format(val_mean_asd))
val_mean_hd /= m
print("Distance de Hausdorff moyenne pour l'ensemble de validation= {}".format(val_mean_hd))

# Enregistre la fusion des images de test et des masques prédits correspondants dans TEST_PRED_PATH
# Calcule le coefficient de Dice moyen pour l'ensemble de test
test_mean_dice = 0
test_mean_asd = 0
test_mean_hd = 0
m = 0
for n, frame in enumerate(test_frames):
    img = 127 * display_test[n, :, :, 0]
    img = img.astype(np.uint8)
    imsave(os.path.join(TEST_PRED_PATH, frame), img)
    mask = imread(os.path.join(TEST_MASK_PATH, frame))
    mask = resize(mask, (IMG_HEIGHT, IMG_WIDTH))
    pred = pred_test[n, :, :, 0].astype(np.float64)
    if not np.amax(pred) == 0:
        dice = metrics_and_losses.dice_coef(mask, pred)
        test_mean_dice += dice
        if not np.amax(mask) == 0:
            euclidian = asd(pred, mask)
            test_mean_asd += euclidian
            hausdorff = hd(pred, mask)
            test_mean_hd += hausdorff
            m += 1
test_mean_dice /= m
print("Coefficient de Dice moyen pour l'ensemble de test= {}".format(test_mean_dice))
test_mean_asd /= m
print("Distance euclidienne moyenne pour l'ensemble de test= {}".format(test_mean_asd))
test_mean_hd /= m
print("Distance de Hausdorff moyenne pour l'ensemble de test= {}".format(test_mean_hd))

# Évaluation des performances
# Calcule des métriques de performance, notamment le coefficient de Dice, la distance euclidienne moyenne (ASD) et la distance de Hausdorff moyenne (HD), pour chaque ensemble.
# Affichage des moyennes de ces métriques pour les ensembles d'entraînement, de validation et de test.